#
# vim:syntax=yaml
#
# Configuration file for pdserv
# This is a YAML document.
#

---
##########################################################################
# Configuration for PdServ
#       - eventhistory: unsigned int; default 100
#               Set the maximum number of events that should be kept in memory
#eventhistory: 100


##########################################################################
# Configuration for the MSR protocol
# Options:
#   bindaddr: string "interface:port", default: ""
#       Host interface and port to bind to, separated by a colon ':'.
#
#       This will open the TCP port on the selected interface(s).
#
#       If the colon is not specified, the string is interpreted as a port
#       specification, with interface empty.
#
#       If the interface is empty, it defaults to all IPv4 interfaces. To
#       specify all IPv6 interfaces, use "::" for the interface part.
#
#       If the port is empty, the server uses the port 2345.
#
#       The host and port are interpreted using getaddrinfo(), which in turn
#       uses inet_aton() or inet_pton() for IPv4 and IPv6 addresses
#       respectively.
#
#       NOTE: Beware that YAML interprets lines ending in a : (no port
#       specification) as a dictionary! In that case, enclose the argument in
#       quotes "".
#
#       Valid examples:
#          ":"          -> interface: empty; port empty
#                          port 2345 on all IPv4 interfaces
#
#          ":::"        -> interface: "::"; port empty
#                          port 2345 on all IPv6 interfaces
#
#          ""           -> see ":"
#
#          "2345"       -> interface: empy; port 2345:
#                          port 2345 on all IPv4 interfaces
#
#          ":::2345"    -> "::" (all IPv6 interfaces); port 2345
#                          port 2345 on all IPv6 interfaces
#
#          "localhost:" -> 127.0.0.1; any available port above 2345
#
#   maxconnections: unsigned int, default: unlimited
#       Limit number of connected clients
#
#   splitvectors: boolean, default: false (DEPRECATED)
#       Present multidimensional variables as individual scalars for older MSR
#       protocol clients
#
#   pathprefix: string, default: '' (DEPRECATED)
#       Prepend a prefix to all variable paths. DO NOT include forward slashes
#       '/' in the path
#
#   parserbufferlimit: unsigned int, default: see below
#       Limit XML input buffer to this value if larger than 1024 bytes.  The
#       default calculated based on largest parameter. In some cases this may
#       be too big for memory. Can also be used to avoid DoS attacks.
#
#   broadcast: boolean, default: false
#       Allow client-to-client broadcast messages.
#
#       Note: This is a very ugly feature of MSR, because it misuses the server
#       as a transport agent for what is really client-to-client communication
#       (aka a kind of primitive chat protocol via MSR). The server is not
#       designed with this purpose in mind. It is a serious security risk as it
#       can be used in a DoS attack. Any client may send broadcasts of any size
#       at any frequency. Other attached clients receive these unsolicied
#       messages and have no means of switching it off. Due to this risk, it
#       has to be switched on explicitely if there is no other option.
#
msr:
    #bindaddr: 2345
    #maxconnections: 20
    splitvectors: 1
    #pathprefix: prefix
    #parserbufferlimit: 0
    #broadcast: 0

##########################################################################
# Access control
#
# PdServ uses the SASL for authentication. It is standardized and allows a
# highly flexible mechanism for authentication. The application is only limited
# to the library's implementation.
#
# PdServ uses Cyrus SASL library. It is a well known and robust implementation
# of SASL. However, its flexibility comes at the cost of configuration. Cyrus
# SASL has various mechanisms to check for authentication, such as checking
# using:
#         - saslauthd (e.g. PAM, LDAP, see man saslauthd)
#         - its own password database (aka sasldb)
#         - IMAP login
#         - local self written plugins
#
# Each of these mechanisms requires a set of configuration options. To see
# what is configured on the target system, use saslpluginviewer
#
# Options:
#   sasl:   Dictionary   Cyrus SASL configuration options.
#                        See "Options for use with Cyrus SASL"
#                        in the Cyrus SASL library documentation:
#                        https://cyrusimap.org
#                                 -> docs -> sasl -> version -> options.php
#   flags:  List or scalar value of flags
#               - mandatory     Require login
#
# To ease the burden on the user, here are 2 scenarios and its configuration:
# 1) Authentication via PAM (aka login). This requires saslauthd to run as
#    a daemon. saslauthd in turn is configurable to use various mechanisms to
#    verify, such as PAM and LDAP. By default, usually PAM is used on modern
#    systems. This mechanism requires that the password is passed from the
#    client to PdServ in plain text, thus it is preferable to use TLS
#    encryption as well.
#
#    - PdServ configuration:
#       sasl:
#           mech_list: PLAIN
#           pwcheck_method: saslauthd
#           #saslauthd_path: /var/run/saslauthd/mux
#
#    Notes:
#    saslauthd_path is only required if the system's default has been changed.
#    Do not use the deprecated LOGIN mechanism
#
# 2) Authentication using password database. This method uses a database where
#    plaintext passwords are stored. Certain mechanisms (such as *-MD5), do
#    not require the password to travel the wire, making TLS optional.
#
#    - PdServ configuration:
#       sasl:
#           mech_list: CRAM-MD5
#           #pwcheck_method: auxprop
#           #sasldb_path: /etc/sasldb2
#    - add/list/delete users using saslpasswd2 and sasldblistusers2
#
#access:
#    sasl:               # SASL configuration
#        mech_list: PLAIN
#        pwcheck_method: saslauthd
#        log_level: 3
#
#    # Flags: string list or scalar
#    #
#    # The following flags are available:
#    #   - mandatory     Login is required by the client; Only TLS handshake is
#    #                   allowed prior to the login
#    flags: mandatory

##########################################################################
# Configuration for persistent parameters
#
# Stores a list of parameters permanently to a Berkeley DB database.
# The database is written when the process is killed as well as cyclically
# using a preset interval.
# When the database exists upon startup, the values are written during the
# initialization phase, i.e. before the task goes into cyclic mode.
#
# Note that only original paramters are considered. Atomized variables
# (msr::splitvectors = 1) are just a view of a vector and as such cannot be
# saved separately.
#
# The Berkeley DB database has the advantage of being a very popular
# and robust database. Standard tools exist to work with it. To see
# which variables are stored, use db_dump:
#       db_dump -p <database_path>
#
# Options:
#       database:       string  # Path to Berkeley DB database
#       interval:       uint    # (optional: default: 0 (never))
#                               # Time in seconds when DB is written.
#                               # This only applies to parameter-signal
#                               # If interval is zero, persistent pairs will
#                               # be written when process shuts exits.
#       cleanup:        uint    # (optional, default: 1) If set,
#                               # keys not matching a persistent variable
#                               # are deleted
#       trace:          uint    # (optional, default: 0) If set,
#                               # the parameter value is logged in
#                               # clear text to the logging channel
#       variables:      list
#         - "/param/path"               # Single parameter
#
#         - parameter: "/param/path"    # Single parameter
#
#         - parameter: "/param/path"    # Parameter signal pair:
#           signal: "/path/to/source"   # The value of signal is stored to
#                                       # the db and reloaded into parameter
#                                       # upon restore.
#                                       # Parameter and Signal must have the
#                                       # same data type and size
#                                       # e.g. Engine hour counter
#                                       # NOTE: manually setting the
#                                       # parameter will discard the signal
#                                       # and will subsequently be treated as
#                                       # if it had no signal.
#
#persistent:
#    interval:   86400  # Daily snapshot of signal-parameter pairs
#    trace: 0
#    cleanup: 1
#    database:   /tmp/pdserv.db
#    variables:
#      - /parameter1
#      - parameter: /parameter2
#      - parameter: /parameter3
#        signal: /signal

##########################################################################
# Transport Layer Security parameters (optional)
#
# See README.tls
#
#tls:
#    # Logging level
#    # Use this only for debugging TLS internals only, as pdserv already has
#    # detailed logging facilities
#    #loglevel: 3
#
#    # Specify server X.509 certificate and key file in PEM format
#    # Required
#    cert: /etc/ssl/certs/server.crt
#    key:  /etc/ssl/certs/server.key
#
#    # Deffie-Hellmann parameter configuration
#    #
#    # Although Deffie-Hellmann parameters can be computed on-the-fly, they are
#    # exponentially expensive to generate and on small platforms may even limit
#    # the security level. It is possible to precompute these parameters and
#    # store them in a file using the "dh" parameter.
#    #
#    # To generate this file, use
#    #   certtool --generate-dh-params --sec-param normal --outfile /path/to/dh.pem
#    # from the gnutls package or
#    #   openssl dhparam -out /path/to/dh.pem 1024
#    #
#    # If the "dh" parameter is unspecified, the parameters are computed using
#    # "dh-bits" with a default of 1024
#    #
#    #dh: /etc/ssl/dh.pem
#    #dh-bits: 1024
#
#    # Set algorithm priority
#    #
#    # This string is used to configure algorithm use and priority. The default
#    # is "NORMAL". See documentation on gnutls_priority_init() in gnutls.html.
#    #
#    # Easy options are:
#    #   NORMAL, PERFORMANCE, SECURE128, SECURE256, EXPORT and NONE
#    #
#    #priority: NORMAL
#
#    # Client verification
#    #
#    # The option contains either:
#    #   - X.509 file in PEM format
#    #   - a directory, that is recursed into, searching for loadable files
#    #
#    # The option may be a scalar or list.
#    #
#    # Using "ca", client verification is turned on who must present a
#    # valid certificate.
#    #ca: /etc/ssl/certs/ca-certificates.crt
#
#    # Certificate revocation list
#    #
#    # The option can contain:
#    #    - X.509 Public Key Id
#    #    - directory: recurse into directory and process all files
#    #    - file: file size == 0: parse file name as X.509 Public Key Id
#    #                       > 0: parse file as X.509 CRL PEM file
#    #
#    # The option may be a scalar or list.
#    #
#    # To get the Public Key Id, use:
#    #   certtool -i < certificate.crt
#    #
#    # Using "crl", client verification is turned on who must present a
#    # valid certificate. Thus this options relies on "ca"
#    #crl:
#    #    - 5b9eb72fda31bb9a0b857ebc8ced6a665a9d5faa
#    #    - /etc/ssl/certs/revoked
#    #    - /etc/ssl/certs/revoked-certs.crl

##########################################################################
# Logging configuration
#
# PDSERV uses the log4cplus package for logging. This is a very powerful
# logging mechanism which is highly configurable. Its design and
# configuration closely resembles the very popular log4j Java tool.
# More information about this configuration can be found by searching
# for "log4j configuration files" and applying it appropriately using the
# example configuration below
# See https://logging.apache.org/log4j/1.2/manual.html
#
# pdserv logs to the following categories:
#   log4cplus.rootLogger: all activities concerning application
#   log4cplus.logger.msr: activities concerning MSR protocol
#   log4cplus.logger.parameter: Parameter changes
#   log4cplus.logger.persistent: Clear text persistent parameter values
#                                when "trace" is true
#   log4cplus.logger.event: Event logging
#
# See http://log4cplus.sourceforge.net/docs/html/classlog4cplus_1_1Appender.html
# for documentation regarding appenders and their options. Click on the
# desired appender in the inheritance diagram
#
logging: |
    log4cplus.rootLogger=INFO, syslog
    #log4cplus.rootLogger=TRACE, console

    ## Here is the parameter channel. Changes to parameters are logged here
    ## If you log parameters to another destination, consider setting the
    ## additivity flag to false so that these messages are not propagated
    ## to the parents (rootLogger) as well.
    #log4cplus.logger.parameter=INFO, parameterlog
    #log4cplus.additivity.parameter=false

    ## Event channel. Application events are sent to this channel
    ## See the comment about additivity above.
    #log4cplus.logger.event=INFO, eventlog
    #log4cplus.additivity.event=false

    ## Example configuration for logging to syslog
    log4cplus.appender.syslog=log4cplus::SysLogAppender
    log4cplus.appender.syslog.ident=pdserv
    log4cplus.appender.syslog.facility=LOCAL0
    log4cplus.appender.syslog.layout=log4cplus::PatternLayout
    log4cplus.appender.syslog.layout.ConversionPattern=%p: %c <%x> - %m%n

    ## Example configuration for logging to a file. More sophisticated
    ## file appenders are RollingFileAppender and DailyRollingFileAppender
    ## capable of truncating the log file size
    log4cplus.appender.parameterlog=log4cplus::FileAppender
    log4cplus.appender.parameterlog.File=/tmp/parameter.log
    log4cplus.appender.parameterlog.layout=log4cplus::PatternLayout
    log4cplus.appender.parameterlog.layout.ConversionPattern=%D %c <%x>: %m%n

    ## Example configuration for logging to a file
    log4cplus.appender.filelog=log4cplus::FileAppender
    log4cplus.appender.filelog.File=/tmp/pdserv.log
    log4cplus.appender.filelog.Append=true
    log4cplus.appender.filelog.layout=log4cplus::PatternLayout
    log4cplus.appender.filelog.layout.ConversionPattern=%D: %-5p %c <%x>: %m%n

    ## Example configuration for logging output to the console
    log4cplus.appender.console=log4cplus::ConsoleAppender
    log4cplus.appender.console.logToStdErr=1
    log4cplus.appender.console.layout=log4cplus::PatternLayout
    log4cplus.appender.console.layout.ConversionPattern=%D %p %c %x: %m%n

    ## This appender does not output anything
    log4cplus.appender.null=log4cplus::NullAppender
