#include <iostream>
#include <cstring>
#include <queue>
#include <sys/time.h>

#include "../src/Debug.h"
#include "../src/TCP.h"

using namespace std;

typedef pthread::AtomicCounter<int> Counter;

struct session: net::TCPSession {
    session(net::TCPServer *server, Counter &c):
        TCPSession(server), counter(c) {
        ++counter;
        detach();
    }
    ~session() {
        sleep(100);
        --counter;
//        cout << __func__ << this << endl;
    }

    void initial() {
        sleep(10);
        cout << "start session " << this << endl;
    }
    void final() {
        cout << "end session " << this << endl;
    }

    void run() {

        do {
            if (isPendingRead(100)) {
                char buf[100];
                ssize_t rv = readData(buf, sizeof(buf));

                if (rv <= 0)
                    return;

                if (writeData(buf, rv) != rv)
                    return;
            }
        } while (true);
    }

    Counter &counter;
};

int main(int argc, char **argv)
{
    Counter counter;

    if (argc < 3)
        return 0;

    net::TCPServer server(5);
    try {
        if (!server.listenTo(argv[1], argv[2]))
            return 0;
    }
    catch (std::string& s) {
        cout << s << endl;
        return 1;
    }

    struct timeval time = {0,0};
    std::queue<struct timeval> timerconnectqueue;

    while (server.isPendingConnection()) {
        if (counter < 20) {
            try {
                session* s = new session(&server, counter);
                cout
                    << "accept "
                    << s->getAddr()
                    << ' ' << s->getPeerName()
                    << endl;
            }
            catch (int i) {
                cout << i << ::strerror(i) << endl;
            }

            ::gettimeofday(&time, 0);
            timerconnectqueue.push(time);
            time.tv_sec -= 1;
            while (timercmp(&timerconnectqueue.front(), &time, <))
                timerconnectqueue.pop();

            if (timerconnectqueue.size() > 10) {
                timersub(&timerconnectqueue.back(),
                        &timerconnectqueue.front(),
                        &time);
                pthread::Thread::sleep(
                        time.tv_sec * 1000 + time.tv_usec/1000);
            }
        }
        else {
            cout << "rejecting" << endl;
            server.reject();
        }
    }

    return 0;
}


