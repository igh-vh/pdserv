/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2017 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef TCP_H
#define TCP_H

#include <stdint.h>
#include <unistd.h>     // ssize_t
#include <ostream>
#include <string>
#include <sys/socket.h> // struct sockaddr
#include <netinet/tcp.h>        // setsockopt()

#include "PThread.h"

namespace net {

    // Open a server socket.
    class TCPSocket {
        public:
            // Returns: true if file descriptor is valid
            operator bool() const;

            void setSockOpt(int level, int name, int val);

            // return: string <IP><sep><port> of listen socket
            std::string getAddr(char sep = ':') const;

        protected:
            /// Constructor
            TCPSocket();
            ~TCPSocket();

            void close();

            // Returns success
            // throws std::string() on error
            bool listenTo(
                    const std::string& interface, const std::string& port,
                    int backlog);

            // return: string of remote host
            // thows: std::string on error
            std::string accept(TCPSocket* server);

            std::string reject();

            bool readable(struct timeval *timeout) const;
            ssize_t readData (      void *buf, size_t len);
            ssize_t writeData(const void *buf, size_t len);

        private:
            int fd;
            struct sockaddr addr;
    };

    class TCPServer: public TCPSocket {
        public:
            TCPServer(int backlog);

            // Returns success
            // throws std::string() on error
            bool listenTo(
                    const std::string& interface, const std::string& port);

            bool isPendingConnection(int msec = -1);
            std::string reject();

        private:
            int backlog;
    };

    class TCPSession: public pthread::Thread, public TCPSocket {
        public:
            // Accept a connection
            // throws: std::string on error
            TCPSession(TCPServer *);

            // return: reverse DNS lookup peer name
            std::string getPeerName() const;

            // Wait for socket read to become ready
            bool isPendingRead(int msec) const;

            // read() from socket
            //
            // return: >=0:     bytes read from socket
            //          <0:     -errno on error
            ssize_t readData (      void *buf, size_t len);

            // write() to socket
            //
            // return: >=0:     bytes written to socket
            //          <0:     -errno on error
            ssize_t writeData(const void *buf, size_t len);

        protected:
            ~TCPSession() {}

        private:
            std::string peerName;
    };
}

#endif // TCP_H
