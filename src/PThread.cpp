/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2017 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "PThread.h"

#include <time.h>
#include <errno.h>

using namespace pthread;

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
RWLock::RWLock()
{
    pthread_rwlockattr_t attr;

    pthread_rwlockattr_init(&attr);
    pthread_rwlock_init(&lock, &attr);
    pthread_rwlockattr_destroy(&attr);
}

////////////////////////////////////////////////////////////////////////////
RWLock::~RWLock()
{
    pthread_rwlock_destroy(&lock);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
Mutex::Mutex()
{
    pthread_mutexattr_t attr;

    pthread_mutexattr_init(&attr);
    pthread_mutex_init(&mutex, &attr);
    pthread_mutexattr_destroy(&attr);
}

////////////////////////////////////////////////////////////////////////////
Mutex::~Mutex()
{
    pthread_mutex_destroy(&mutex);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
Thread::Thread()
{
    pthread_attr_init(&attr);
}

////////////////////////////////////////////////////////////////////////////
Thread::~Thread()
{
    pthread_attr_destroy(&attr);
}

////////////////////////////////////////////////////////////////////////////
int Thread::start()
{
    if (was_started)
        return 0;

    const int ans = pthread_create(&id, &attr, &start_routine, this);
    if (ans == 0)
        was_started = true;
    return ans;
}

////////////////////////////////////////////////////////////////////////////
int Thread::detach()
{
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    return start();
}

////////////////////////////////////////////////////////////////////////////
void Thread::terminate() noexcept
{
    if (was_started)
        pthread_cancel(id);
}

////////////////////////////////////////////////////////////////////////////
void *Thread::join() noexcept
{
    // join() may be called in a dtor, so make sure to not throw
    int cancel_state;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state);
    void *retval = nullptr;
    if (was_started)
        pthread_join(id, &retval);
    pthread_setcancelstate(cancel_state, nullptr);
    return retval;
}

////////////////////////////////////////////////////////////////////////////
void *Thread::start_routine(void *arg)
{
    Thread* self = reinterpret_cast<Thread*>(arg);

    // RAII wrapper to run final() in case of pthread_cancel
    struct ThreadFinalizer
    {
        Thread* thread_;
        ThreadFinalizer(Thread* thread) : thread_(thread) {}
        ~ThreadFinalizer()
        {
            // disable cancelation, because terminate() will
            // otherwise lead to std::terminate
            pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, nullptr);
            thread_->final();
            int state;
            pthread_attr_getdetachstate(&thread_->attr, &state);
            if (state == PTHREAD_CREATE_DETACHED)
                delete thread_;
        }
    } finalizer(self);

    self->initial();
    self->run();

    return 0;
}

////////////////////////////////////////////////////////////////////////////
void Thread::sleep(int msec)
{
    struct timespec delay;

    delay.tv_sec = msec / 1000;
    delay.tv_nsec = (msec - delay.tv_sec * 1000) * 1000000;

    while (::nanosleep(&delay, &delay) and errno == EINTR)
        ;
}
