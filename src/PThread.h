/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2017 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PTHREAD_H
#define PTHREAD_H

#include <pthread.h>

namespace pthread {

    //////////////////////////////////////////////////////////////////////
    class RWLock {
        public:
            RWLock();
            ~RWLock();

            void writelock() {
                pthread_rwlock_wrlock(&lock);
            }

            void readlock() {
                pthread_rwlock_rdlock(&lock);
            }

            void unlock() {
                pthread_rwlock_unlock(&lock);
            }

        private:
            pthread_rwlock_t lock;
    };

    class ReadLock {
        public:
            ReadLock(RWLock &l): lock(l) {
                lock.readlock();
            }

            ~ReadLock() {
                lock.unlock();
            }

        private:
            RWLock &lock;
    };

    class WriteLock {
        public:
            WriteLock(RWLock &l): lock(l) {
                lock.writelock();
            }

            ~WriteLock() {
                lock.unlock();
            }

        private:
            RWLock &lock;
    };


    //////////////////////////////////////////////////////////////////////
    class Mutex {
        public:
            Mutex();
            ~Mutex();

            void lock() {
                pthread_mutex_lock(&mutex);
            }

            void unlock() {
                pthread_mutex_unlock(&mutex);
            }


        private:
            pthread_mutex_t mutex;
    };

    class MutexLock {
        public:
            MutexLock(Mutex &m): mutex(m) {
                mutex.lock();
            }

            ~MutexLock() {
                mutex.unlock();
            }

        private:
            Mutex &mutex;
    };

    //////////////////////////////////////////////////////////////////////
    template <class T>
        class AtomicCounter {
            public:
                AtomicCounter(const T &initval = T()) {
                    counter = initval;
                }

                T operator++() {
                    MutexLock l(mutex);
                    return ++counter;
                }

                T operator--() {
                    MutexLock l(mutex);
                    return --counter;
                }

                operator T() {
                    MutexLock l(mutex);
                    return counter;
                }

            private:
                T counter;
                Mutex mutex;
        };

    //////////////////////////////////////////////////////////////////////
    class Thread {
        public:
            Thread();
            virtual ~Thread();

            int start();
            int detach();
            void terminate() noexcept;
            void *join() noexcept;

            static void sleep(int msec);

        protected:
            virtual void initial() {};
            virtual void run() = 0;
            virtual void final() {};

        private:
            static void *start_routine(void *arg);

            pthread_attr_t attr;
            pthread_t id;
            bool was_started = false;
    };
}

#endif // PTHREAD_H
