/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "../Debug.h"

#include "Event.h"
#include "Main.h"

//////////////////////////////////////////////////////////////////////
Event::Event( Main *main, const char* path,
        size_t nelem, const char * const *messages):
    PdServ::Event(path, nelem, messages),
    main(main),
    m_state(new Priority[nelem])
{
    std::fill_n(m_state, nelem, Reset);
}

//////////////////////////////////////////////////////////////////////
Event::~Event()
{
    delete[] m_state;
}

//////////////////////////////////////////////////////////////////////
void Event::set(size_t elem, Priority prio, const timespec *t) const
{
    if (m_state[elem] == prio)
        return;

    if (prio == Reset)
        main->resetEvent(this, elem, t);
    else
        main->setEvent(this, elem, prio, t);

    m_state[elem] = prio;
}

//////////////////////////////////////////////////////////////////////
void Event::reset(size_t elem, const timespec *t) const
{
    set(elem, Reset, t);
}
