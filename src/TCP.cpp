/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2017 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "TCP.h"
#include "Debug.h"

#include <cerrno>
#include <cstring>      // memset()
#include <stdexcept>
#include <sstream>
#include <unistd.h>     // close()
#include <sys/select.h> // select()
#include <sys/time.h>   // struct timeval

// socket stuff: getaddrinfo(), gai_strerror(), connect(), bind(),
// setsockopt(), inet_ntop()
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

using namespace net;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
TCPSocket::TCPSocket(): fd(-1)
{
}

/////////////////////////////////////////////////////////////////////////////
TCPSocket::~TCPSocket()
{
    close();
}

/////////////////////////////////////////////////////////////////////////////
bool TCPSocket::listenTo(
        const std::string& interface, const std::string& port, int backlog)
{
    struct addrinfo hints;
    ::memset(&hints, 0, sizeof hints);
    hints.ai_family = interface.empty() ? AF_INET : AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM; /* TCP socket */
    hints.ai_protocol = 0;           /* Any protocol */
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *result;
    int s = ::getaddrinfo(interface.empty() ? NULL : interface.c_str(),
            port.c_str(), &hints, &result);
    if (s != 0) {
        std::ostringstream os;
        os << "getaddrinfo("
            << "address=" << interface << ", "
            << "port=" << port << "): "
            << ::gai_strerror(s);
        throw os.str();
    }

    close();

    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully connect(2).
       If socket(2) (or connect(2)) fails, we (close the socket
       and) try the next address. */

    const char* errfunc = 0;
    for (struct addrinfo* rp = result;
            rp and !errfunc and fd < 0; rp = rp->ai_next) {

        fd = ::socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (fd < 0)
            continue;

        // Reuse port
        int optval = 1;
        if (::setsockopt(fd,
                    SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval)) {
            errfunc = "setsockopt";
        }
        else if (!::bind(fd, rp->ai_addr, rp->ai_addrlen)) {
            if (!::listen(fd, backlog)) {
                addr = *rp->ai_addr;
            }
            else {
                errfunc = "listen";
            }
        }
        else
            close();
    }

    ::freeaddrinfo(result);

    if (errfunc)
        throw std::string(errfunc).append("(): ").append(::strerror(errno));

    return fd >= 0;
}

/////////////////////////////////////////////////////////////////////////////
void TCPSocket::close()
{
    int cancel_state;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state);
    if (fd >= 0)
        ::close(fd);
    fd = -1;
    pthread_setcancelstate(cancel_state, nullptr);
}

/////////////////////////////////////////////////////////////////////////////
TCPSocket::operator bool() const
{
    return fd >= 0;
}

/////////////////////////////////////////////////////////////////////////////
bool TCPSocket::readable(struct timeval *timeout) const
{
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(fd, &readfds);
    return ::select(fd + 1, &readfds, 0, 0, timeout) > 0;
}

/////////////////////////////////////////////////////////////////////////////
std::string TCPSocket::accept(TCPSocket* server)
{
    struct sockaddr_storage sa;
    socklen_t len = sizeof sa;
    struct sockaddr* addr = (struct sockaddr*)&sa;

    fd = ::accept(server->fd, addr, &len);
    if (fd < 0)
        throw std::string("accept() on ")
            .append(server->getAddr())
            .append(": ")
            .append(strerror(errno));

    this->addr = *addr;

    // Set server FQDN
    char host[NI_MAXHOST];
    if (!::getnameinfo(addr, len,
                host, NI_MAXHOST, 0, 0, NI_NUMERICSERV))
        return host;

    return "Unknown";
}

/////////////////////////////////////////////////////////////////////////////
void TCPSocket::setSockOpt(int level, int opt, int val)
{
    if (::setsockopt(fd, level, opt, &val, sizeof val)) {
        throw std::string("setsockopt() on ")
            .append(getAddr())
            .append(": ")
            .append(strerror(errno));
    }
}

/////////////////////////////////////////////////////////////////////////////
std::string TCPSocket::reject()
{
    TCPSocket s;
    std::string name = s.accept(this);

    return s.getAddr().append(" (").append(name).append(1,')');
}

/////////////////////////////////////////////////////////////////////////////
std::string TCPSocket::getAddr(char sep) const
{
    std::string host;
    uint16_t port = 0;

    switch (addr.sa_family) {
        case AF_INET:
            {
                struct sockaddr_in *s = (struct sockaddr_in*)&addr;
                char str[INET_ADDRSTRLEN];
                if (::inet_ntop(AF_INET, &s->sin_addr, str, sizeof str)) {
                    host = str;
                    port = ntohs(s->sin_port);
                }
            }
            break;
        case AF_INET6:
            {
                struct sockaddr_in6 *s = (struct sockaddr_in6*)&addr;
                char str[INET6_ADDRSTRLEN];
                if (::inet_ntop(AF_INET6, &s->sin6_addr, str, sizeof str)) {
                    host = str;
                    port = ntohs(s->sin6_port);
                }
            }
            break;
    }

    std::ostringstream os;
    os << port;
    return host + sep + os.str();
}

/////////////////////////////////////////////////////////////////////////////
ssize_t TCPSocket::readData(void *buf, size_t len)
{
    int rv = ::read(fd, buf, len);
    return rv >= 0 ? rv : -errno;;
}

/////////////////////////////////////////////////////////////////////////////
ssize_t TCPSocket::writeData(const void *buf, size_t len)
{
    int rv = ::send(fd, buf, len, MSG_NOSIGNAL);
    return rv >= 0 ? rv : -errno;;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
TCPServer::TCPServer(int backlog): backlog(backlog)
{
}

/////////////////////////////////////////////////////////////////////////////
bool TCPServer::listenTo(
        const std::string& interface, const std::string& port)
{
    return TCPSocket::listenTo(interface, port, backlog);
}

/////////////////////////////////////////////////////////////////////////////
bool TCPServer::isPendingConnection(int msec)
{
    struct timeval timeout;
    timeout.tv_sec = msec / 1000;
    timeout.tv_usec = 1000*(msec - 1000*timeout.tv_sec);

    return readable(msec < 0 ? 0 : &timeout);
}

/////////////////////////////////////////////////////////////////////////////
std::string TCPServer::reject()
{
    return TCPSocket::reject();
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
TCPSession::TCPSession(TCPServer *server)
{
    peerName = accept(server);
}

/////////////////////////////////////////////////////////////////////////////
std::string TCPSession::getPeerName() const
{
    return peerName;
}

/////////////////////////////////////////////////////////////////////////////
bool TCPSession::isPendingRead(int msec) const
{
    struct timeval timeval;
    timeval.tv_sec = msec / 1000;
    timeval.tv_usec = (msec - timeval.tv_sec*1000) * 1000;

    return readable(msec < -1 ? 0 : &timeval);
}

/////////////////////////////////////////////////////////////////////////////
ssize_t TCPSession::readData(void *buf, size_t len)
{
    return TCPSocket::readData(buf, len);
}

/////////////////////////////////////////////////////////////////////////////
ssize_t TCPSession::writeData(const void *buf, size_t len)
{
    return TCPSocket::writeData(buf, len);
}
