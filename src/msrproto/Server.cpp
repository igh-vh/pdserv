/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "config.h"

#include "Server.h"
#include "Channel.h"
#include "TimeSignal.h"
#include "StatSignal.h"
#include "Parameter.h"
#include "Session.h"
#include "../Main.h"
#include "../Task.h"
#include "../Signal.h"
#include "../Parameter.h"
#include "../TCP.h"

#include <string>
#include <cerrno>
#include <algorithm>
#include <log4cplus/loggingmacros.h>

using namespace MsrProto;

template <class T>
std::string toString(T val)
{
    std::ostringstream os;
    os << val + 0;      // val + 0 is a neat way of preventing ostream
                        // from printing characters when processing
                        // char values
    return os.str();
}

/////////////////////////////////////////////////////////////////////////////
Server::Server(const PdServ::Main *main, const PdServ::Config &config):
    main(main),
    log(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("msr"))),
    state_(Init)
{
    server = 0;
    maxInputBufferSize = 0U;

    bindaddr = config["bindaddr"].toString();
    m_broadcast = config["broadcast"].toUInt();

    if (bindaddr.empty()) {
        // It is difficult to multi-host in future using separate host and
        // port configurations. It is better to use one configuration
        // parameter, 'bindaddr' instead.
        const std::string host = config["bindhost"].toString();
        const std::string port = config["port"].toString();

        if (!port.empty() or !host.empty()) {
            LOG4CPLUS_WARN_STR(log,
                    LOG4CPLUS_TEXT(
                        "'bindhost' and 'port' are deprecated. "
                        "Use 'bindaddr' instead."));
            bindaddr = host + ':' + port;
        }
    }

    itemize = config["splitvectors"].toUInt();

    insertRoot = &variableDirectory;
    if (config["pathprefix"])
        insertRoot = variableDirectory.create(
                config["pathprefix"].toString(main->name));

    maxConnections = config["maxconnections"].toUInt(~0U);

    for (std::list<const PdServ::Task*> taskList(main->getTasks());
            taskList.size(); taskList.pop_front())
        createChannels(insertRoot, taskList.front());

    createParameters(insertRoot);

    unsigned int bufLimit = config["parserbufferlimit"].toUInt();
    if (bufLimit > 1024 and maxInputBufferSize > bufLimit)
        maxInputBufferSize = bufLimit;
    LOG4CPLUS_INFO(log,
            LOG4CPLUS_TEXT("Limiting XML input buffer to ")
            << maxInputBufferSize
            << LOG4CPLUS_TEXT(" bytes"));

    // wait for background thread to boot
    {
        std::unique_lock<std::mutex> lck(mutex);
        start();
        startup_cv.wait(lck, [this]{ return state_ != Init; });
    }
    if (state_ != Running)
    {
        // dtor is not called, so cleanup.
        terminate();
        join();
        throw std::runtime_error("starting pdserv failed");
    }
}

/////////////////////////////////////////////////////////////////////////////
Server::~Server()
{
    terminate();
    join();

    // Parameters and Channels are deleted when variable tree
    // is deleted
}

/////////////////////////////////////////////////////////////////////////////
std::string Server::getAddr(char sep) const
{
    return server->getAddr(sep);
}

/////////////////////////////////////////////////////////////////////////////
void Server::createChannels(DirectoryNode* baseDir, const PdServ::Task* task)
{
    Channel* c;

    LOG4CPLUS_TRACE(log,
            LOG4CPLUS_TEXT("Create channels for task ") << task->index
            << LOG4CPLUS_TEXT(", Ts=") << task->sampleTime);

    std::list<const PdServ::Signal*> signals(task->getSignals());

    // Reserve at least signal count and additionally 4 Taskinfo signals
    channels.reserve(channels.size() + signals.size() + 4);

    for (; signals.size(); signals.pop_front()) {
        const PdServ::Signal *signal = signals.front();

//        LOG4CPLUS_TRACE(log, LOG4CPLUS_TEXT(signal->path)
//                << signal->dtype.name);
        PdServ::DataType::Iterator<CreateChannel>(
                signal->dtype, signal->dim,
                CreateChannel(this, baseDir, signal));
    }

    DirectoryNode* taskInfo = variableDirectory.create("Taskinfo");

    std::ostringstream os;
    os << task->index;
    DirectoryNode* t = taskInfo->create(os.str());

    c = new TimeSignal(task, channels.size());
    channels.push_back(c);
    t->insert(c, "TaskTime");

    c = new StatSignal(task, StatSignal::ExecTime, channels.size());
    channels.push_back(c);
    t->insert(c, "ExecTime");

    c = new StatSignal(task, StatSignal::Period, channels.size());
    channels.push_back(c);
    t->insert(c, "Period");

    c = new StatSignal(task, StatSignal::Overrun, channels.size());
    channels.push_back(c);
    t->insert(c, "Overrun");
}

/////////////////////////////////////////////////////////////////////////////
void Server::createParameters(DirectoryNode* baseDir)
{
    LOG4CPLUS_TRACE_STR(log,
            LOG4CPLUS_TEXT("Create parameters"));

    std::list<const PdServ::Parameter*> mainParam(main->getParameters());

    parameters.reserve(parameters.size() + mainParam.size());

    for (; mainParam.size(); mainParam.pop_front()) {
        const PdServ::Parameter *param = mainParam.front();

        // Calculate maximum dataspace requirement to change a parameter
        // in bytes.
        //
        // The following table shows the requirement for selected data types:
        // Type   | bytes | infr. |   mant |  exp |  total
        // -------+-------+-------+--------+------+-------
        // double |     8 |     3 |     16 |    4 |     23
        // int64  |     8 |     1 |     19 |      |     20
        // single |     4 |     3 |      7 |    4 |     14
        // int32  |     4 |     1 |     10 |      |     11
        // int16  |     2 |     1 |      5 |      |      6
        // int8   |     1 |     1 |      3 |      |      4
        //
        // where:
        //     bytes: sizeof(type)
        //     infr:  characters for infrastrucure (sign, exp, decimal point,
        //            field separator)
        //     mant:  characters for mantissa representation
        //     exp:   characters for exponent representation
        //
        // Thus it is safe to caculate with 4 characters per raw byte
        maxInputBufferSize =
            std::max(maxInputBufferSize,
                    param->memSize * 4U + param->path.size() + 512);

        PdServ::DataType::Iterator<CreateParameter>(
                param->dtype, param->dim,
                CreateParameter(this, baseDir, param));
    }
}

/////////////////////////////////////////////////////////////////////////////
size_t Server::getMaxInputBufferSize() const
{
    return maxInputBufferSize;
}

/////////////////////////////////////////////////////////////////////////////
void Server::initial()
{
    std::unique_lock<std::mutex> lck(mutex);
    struct NotifyAtLeave {
        std::condition_variable& cv_;
        NotifyAtLeave(std::condition_variable& cv) : cv_(cv) {}
        ~NotifyAtLeave() { cv_.notify_all();}
    } const wakeup_parent_thread(startup_cv);

    LOG4CPLUS_INFO_STR(log, LOG4CPLUS_TEXT("Initializing MSR server"));

    // Split bindaddr into interface and port
    std::string port, interface;
    size_t colon = bindaddr.rfind(':');
    if (colon == std::string::npos) {
        // No colon specified, assume bindaddr specifies port only
        port = bindaddr;
    }
    else {
        // split interface:port
        interface = bindaddr.substr(0, colon);
        port = bindaddr.substr(colon+1);
    }

    server = new net::TCPServer(5);

    if (port.empty()) {
        port = "2345";
    }
    if (listenTo(interface, port) == 1) {
        state_ = Running;
    } else {
        state_ = Stopped;
        LOG4CPLUS_ERROR(
                log,
                LOG4CPLUS_TEXT("Port in \"") << LOG4CPLUS_STRING_TO_TSTRING(
                        bindaddr) << LOG4CPLUS_TEXT("\" already in use"));
    }

}

/////////////////////////////////////////////////////////////////////////////
// Returns:
//      1: successful
//      0: could not connect because port is in use
//     -1: some error occurred
int Server::listenTo(const std::string& interface, const std::string& port)
{
    try {
        if (!server->listenTo(interface, port))
            return 0;

        LOG4CPLUS_INFO(log,
                LOG4CPLUS_TEXT("Server started on ")
                << LOG4CPLUS_STRING_TO_TSTRING(server->getAddr()));

        return 1;
    }
    catch (const std::string& s) {
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Socket failure: ")
                << LOG4CPLUS_STRING_TO_TSTRING(s));
        return -1;
    }
}

/////////////////////////////////////////////////////////////////////////////
void Server::final()
{
    delete server;

    state_ = Stopped;

    while (true) {
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (sessions.empty())
                break;
        }
        Thread::sleep(100);
    }

    LOG4CPLUS_INFO_STR(log, LOG4CPLUS_TEXT("Exiting MSR server"));
}

/////////////////////////////////////////////////////////////////////////////
void Server::run()
{
    while (active() and server->isPendingConnection()) {

        if (sessions.size() == maxConnections) {
            std::string name = server->reject();
            LOG4CPLUS_WARN(log,
                    LOG4CPLUS_TEXT("Maximum connection count reached. "
                        "Rejecting client connection attempt from ")
                    << LOG4CPLUS_STRING_TO_TSTRING(name));
            continue;
        }

        Session* session = 0;
        try {
            std::unique_lock<std::mutex> lock(mutex);

            LOG4CPLUS_DEBUG_STR(log,
                    LOG4CPLUS_TEXT("New client connection"));
            session = new Session(this, server);

            session->setSockOpt(SOL_SOCKET, SO_KEEPALIVE, 1);
            session->setSockOpt(SOL_TCP, TCP_KEEPIDLE, 300);

            sessions.insert(session);
        }
        catch (const std::string& s) {
            LOG4CPLUS_FATAL(log,
                    LOG4CPLUS_TEXT("Socket failure: ")
                    << LOG4CPLUS_STRING_TO_TSTRING(s));
            if (session)
                session->close();
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
void Server::broadcast(Session *s, const struct timespec& ts,
        const std::string& action, const std::string &message)
{
    if (!m_broadcast)
        return;

    std::unique_lock<std::mutex> lock(mutex);
    for (std::set<Session*>::iterator it = sessions.begin();
            it != sessions.end(); ++it)
        (*it)->broadcast(s, ts, action, message);
}

/////////////////////////////////////////////////////////////////////////////
void Server::sessionClosed(Session *s)
{
    std::unique_lock<std::mutex> lock(mutex);
    sessions.erase(s);
}

/////////////////////////////////////////////////////////////////////////////
void Server::getSessionStatistics(
        std::list<PdServ::SessionStatistics>& stats) const
{
    std::unique_lock<std::mutex> lock(mutex);
    for (std::set<Session*>::iterator it = sessions.begin();
            it != sessions.end(); ++it) {
        PdServ::SessionStatistics s;
        (*it)->getSessionStatistics(s);
        stats.push_back(s);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Server::setAic(const Parameter *p)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (std::set<Session*>::iterator it = sessions.begin();
            it != sessions.end(); ++it)
        (*it)->setAIC(p);
}

/////////////////////////////////////////////////////////////////////////////
void Server::parameterChanged(const PdServ::Parameter *mainParam,
        size_t offset, size_t count,
        const char* data, const struct timespec* time)
{
    const Parameter *p = find(mainParam);

    std::unique_lock<std::mutex> lock(mutex);
    for (std::set<Session*>::iterator it = sessions.begin();
            it != sessions.end(); ++it)
        p->inform(*it, offset, offset + count, data, time);
}

/////////////////////////////////////////////////////////////////////////////
const Channel* Server::getChannel(size_t n) const
{
    return n < channels.size() ? channels[n] : 0;
}

/////////////////////////////////////////////////////////////////////////////
const Server::Channels& Server::getChannels() const
{
    return channels;
}

/////////////////////////////////////////////////////////////////////////////
void Server::listDir(PdServ::Session *session,
        XmlElement& xml, const std::string& path, bool hex, bool derived) const
{
    variableDirectory.list(session, xml, path, 0, hex, derived);
}

/////////////////////////////////////////////////////////////////////////////
const Parameter* Server::getParameter(size_t n) const
{
    return n < parameters.size() ? parameters[n] : 0;
}

/////////////////////////////////////////////////////////////////////////////
const Server::Parameters& Server::getParameters() const
{
    return parameters;
}

/////////////////////////////////////////////////////////////////////////////
const Parameter *Server::find( const PdServ::Parameter *p) const
{
    return parameterMap.find(p)->second;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
Server::CreateVariable::CreateVariable(
        Server* server, DirectoryNode* baseDir, const PdServ::Variable* var):
    parent(0), server(server), baseDir(baseDir), var(var)
{
}

/////////////////////////////////////////////////////////////////////////////
Server::CreateVariable::CreateVariable(const CreateVariable& other):
    parent(&other), server(other.server),
    baseDir(other.baseDir), var(other.var)
{
}

/////////////////////////////////////////////////////////////////////////////
std::string Server::CreateVariable::path() const
{
    return (parent ? parent->path() : var->path)
        + std::string(!name.empty(), '/') + name;
}

/////////////////////////////////////////////////////////////////////////////
void Server::CreateVariable::newDimension(
        const PdServ::DataType& /*dtype*/,
        const PdServ::DataType::DimType& /*dim*/,
        size_t /*dimIdx*/, size_t elemIdx,
        CreateVariable& /*c*/, size_t /*offset*/)
{
    std::ostringstream os;
    os << elemIdx;
    name = os.str();
}

/////////////////////////////////////////////////////////////////////////////
void Server::CreateVariable::newField(const PdServ::DataType::Field* field,
        CreateVariable& /*c*/, size_t /*offset*/)
{
    name = field->name;
}

/////////////////////////////////////////////////////////////////////////////
bool Server::CreateVariable::newVariable(
        const PdServ::DataType& dtype,
        const PdServ::DataType::DimType& dim,
        size_t dimIdx, size_t elemIdx, size_t offset) const
{
    bool rv = false;

    if (dtype.isPrimary()) {
        if ((dimIdx == 0 and elemIdx == 0) or !server->itemize) {
            // Root element of a primary variable
            rv = createVariable(dtype, dim, offset);
        }
        else if (dimIdx == dim.size()) {
            // Leaf node with only one element
            static const size_t d = 1;
            rv = createVariable(dtype,
                    PdServ::DataType::DimType(1, &d), offset);
        }
    }

    return rv;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
Server::CreateChannel::CreateChannel(
        Server* server, DirectoryNode* baseDir, const PdServ::Signal* s):
    CreateVariable(server, baseDir, s)
{
}

/////////////////////////////////////////////////////////////////////////////
bool Server::CreateChannel::createVariable(const PdServ::DataType& dtype,
        const PdServ::DataType::DimType& dim, size_t offset) const
{
    Channel *c = new Channel(static_cast<const PdServ::Signal*>(var),
            server->channels.size(), dtype, dim, offset);
    bool rv;

    if (server->itemize) {
        char hidden = 0;
        char persist = 0;
        baseDir->traditionalPathInsert(c, path(), hidden, persist);
        c->hidden = hidden == 'c' or hidden == 'k' or hidden == 1;

        rv = c->hidden;
    }
    else {
        baseDir->pathInsert(c, path());
        rv = true;
    }

    server->channels.push_back(c);
    return rv;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
Server::CreateParameter::CreateParameter(
        Server* server, DirectoryNode* baseDir, const PdServ::Parameter* p):
    CreateVariable(server, baseDir, p), parentParameter(0)
{
}

/////////////////////////////////////////////////////////////////////////////
bool Server::CreateParameter::createVariable(const PdServ::DataType& dtype,
        const PdServ::DataType::DimType& dim, size_t offset) const
{
    Parameter *p = new Parameter(static_cast<const PdServ::Parameter*>(var),
            server->parameters.size(), dtype, dim, offset, parentParameter);
    bool rv;

    server->parameters.push_back(p);

    if (server->itemize) {
        char hidden = 0;
        char persist = 0;
        baseDir->traditionalPathInsert(p, path(), hidden, persist);
        p->hidden = hidden == 'p' or hidden == 1;
        p->persistent = persist;

        rv = p->hidden;
    }
    else {
        baseDir->pathInsert(p, path());
        rv = true;
    }

    if (!parentParameter)
        server->parameterMap[static_cast<const PdServ::Parameter*>(var)] = p;
    parentParameter = p;

    return rv;
}
