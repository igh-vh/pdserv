/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <streambuf>
#include <cerrno>       // ENAMETOOLONG
#include <climits>      // HOST_NAME_MAX
#include <unistd.h>     // gethostname
#include <log4cplus/ndc.h>
#include <log4cplus/loggingmacros.h>
#include <endian.h>

#include "config.h"

#include "../Debug.h"

#include "../Main.h"
#include "../Task.h"
#include "../Signal.h"
#include "../Parameter.h"
#include "../DataType.h"
#include "../Event.h"

#include "Session.h"
#include "Server.h"
#include "Event.h"
#include "Channel.h"
#include "Parameter.h"
#include "XmlElement.h"
#include "XmlParser.h"
#include "SubscriptionManager.h"

using namespace MsrProto;

struct ParameterData {
    ParameterData(const Parameter* p,
            const char* buf, const struct timespec* t)
        : parameter(p), data(new char[p->memSize]), time(*t) {
            std::copy(buf, buf + p->memSize, data);
    }
    ~ParameterData() { delete[] data; }

    const Parameter* const parameter;
    char * const data;
    const struct timespec time;
};

/////////////////////////////////////////////////////////////////////////////
Session::Session( Server *server, net::TCPServer* tcp):
    TCPSession(tcp),
    PdServ::Session(server->main, server->log), server(server),
    xmlstream(static_cast<PdServ::Session*>(this))
{
    inBytes = 0;
    outBytes = 0;

    timeTask = 0;

    std::list<const PdServ::Task*> taskList(main->getTasks());
    subscriptionManager.reserve(taskList.size());
    for (; taskList.size(); taskList.pop_front()) {
        const PdServ::Task *task = taskList.front();

        subscriptionManager.push_back(new SubscriptionManager(this, task));

        if (!timeTask or timeTask->task->sampleTime > task->sampleTime)
            timeTask = subscriptionManager.back();
    }

    // Setup some internal variables
    writeAccess = false;
    echoOn = false;     // FIXME: echoOn is not yet implemented
    quiet = false;
    polite = false;
    aicDelay = 0;
    parameterMonitor = false;
    eventId = main->getCurrentEventId();

    xmlstream.imbue(std::locale::classic());

    detach();
}

/////////////////////////////////////////////////////////////////////////////
Session::~Session()
{
    server->sessionClosed(this);

    for (SubscriptionManagerVector::iterator it = subscriptionManager.begin();
            it != subscriptionManager.end(); ++it)
        delete *it;
}

/////////////////////////////////////////////////////////////////////////////
void Session::close()
{
    TCPSession::close();
}

/////////////////////////////////////////////////////////////////////////////
size_t Session::getReceiveBufSize() const
{
    return std::max(server->getMaxInputBufferSize() + 1024UL, 8192UL);
}

/////////////////////////////////////////////////////////////////////////////
void Session::getSessionStatistics(PdServ::SessionStatistics &stats) const
{
    std::ostringstream os;
    if (remoteHostName.empty())
        stats.remote = getAddr();
    else
        stats.remote = remoteHostName + " (" + getAddr() +')';
    stats.client = client;
    stats.countIn = inBytes;
    stats.countOut = outBytes;
    stats.connectedTime = connectedTime;
}

/////////////////////////////////////////////////////////////////////////////
const struct timespec *Session::getTaskTime (const PdServ::Task* task) const
{
    return subscriptionManager[task->index]->taskTime;
}

/////////////////////////////////////////////////////////////////////////////
const PdServ::TaskStatistics *Session::getTaskStatistics (
        const PdServ::Task* task) const
{
    return subscriptionManager[task->index]->taskStatistics;
}

/////////////////////////////////////////////////////////////////////////////
void Session::broadcast(Session *, const struct timespec& ts,
        const std::string& action, const std::string &message)
{
    pthread::MutexLock lock(mutex);

    Broadcast *b = new Broadcast;
    b->ts = ts;
    b->action = action;
    b->message = message;
    broadcastList.push_back(b);
}

/////////////////////////////////////////////////////////////////////////////
void Session::setAIC(const Parameter *p)
{
    pthread::MutexLock lock(mutex);
    aic.insert(p->mainParam);

    if (!aicDelay)
        aicDelay = 5;  // 2Hz AIC
}

/////////////////////////////////////////////////////////////////////////////
void Session::parameterChanged(const Parameter *p,
                const char* data, const struct timespec* time)
{
    pthread::MutexLock lock(mutex);

    if (!parameterMonitor and parameterMonitorSet.empty())
        changedParameter.insert(p);
    else if (p->memSize == p->variable->memSize
            and (parameterMonitorSet.find(p) != parameterMonitorSet.end()
                or parameterMonitor))
        parameterMonitorData.push(new ParameterData(p, data, time));
}

/////////////////////////////////////////////////////////////////////////////
void Session::initial()
{
    TCPSession::initial();

    log4cplus::getNDC().push(LOG4CPLUS_STRING_TO_TSTRING(getAddr()));

    LOG4CPLUS_INFO(server->log,
            LOG4CPLUS_TEXT("New session from ")
            << LOG4CPLUS_STRING_TO_TSTRING(getPeerName()));

    // Get the hostname
    char hostname[HOST_NAME_MAX+1];
    if (gethostname(hostname, HOST_NAME_MAX)) {
        if (errno == ENAMETOOLONG)
            hostname[HOST_NAME_MAX] = '\0';
        else
            strcpy(hostname,"unknown");
    }

    // Greet the new client
    {
        XmlElement greeting(createElement("connected"));
        XmlElement::Attribute(greeting, "name") << "MSR";
        XmlElement::Attribute(greeting, "host")
            << reinterpret_cast<const char*>(hostname);
        XmlElement::Attribute(greeting, "app") << main->name;
        XmlElement::Attribute(greeting, "appversion") << main->version;
        XmlElement::Attribute(greeting, "version") << MSR_VERSION;
        XmlElement::Attribute(greeting, "features") << MSR_FEATURES
            << ",login,history,xsap"
#ifdef GNUTLS_FOUND
            << (main->tlsReady() ? ",tls" : "")
#endif
            ;

        XmlElement::Attribute(greeting, "endian")
#if __BYTE_ORDER == __BIG_ENDIAN
            << "big"
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            << "little"
#endif
            ;

        if (main->loginMandatory())
            XmlElement::Attribute(greeting, "login") << "mandatory";

        XmlElement::Attribute(greeting, "recievebufsize")
            << getReceiveBufSize();
    }

    if (main->loginMandatory())
        authenticate(0);
}

/////////////////////////////////////////////////////////////////////////////
void Session::final()
{
    LOG4CPLUS_INFO_STR(server->log, LOG4CPLUS_TEXT("Finished session"));
    log4cplus::getNDC().remove();
}

/////////////////////////////////////////////////////////////////////////////
void Session::run()
{
    XmlParser parser(getReceiveBufSize());

    while (server->active()) {
        if (!xmlstream.good()) {
            LOG4CPLUS_FATAL_STR(server->log,
                    LOG4CPLUS_TEXT("Error occurred in output stream"));
            return;
        }

        xmlstream.flush();

        if (isPendingRead(40)) {
            if (!parser.read(static_cast<PdServ::Session*>(this))) {
                if (PdServ::Session::eof())
                    return;

                if (parser.invalid()) {
                    LOG4CPLUS_FATAL_STR(server->log,
                            LOG4CPLUS_TEXT(
                                "Input buffer overflow in XML parser"));
                    return;
                }
            }
//                LOG4CPLUS_TRACE(server->log,
//                        LOG4CPLUS_TEXT("Rx: ")
//                        << LOG4CPLUS_STRING_TO_TSTRING(
//                            std::string(inbuf.bufptr(), n)));

            while (parser) {
                parser.getString("id", commandId);
                processCommand(&parser);

                if (!commandId.empty()) {
                    XmlElement ack(createElement("ack"));
                    XmlElement::Attribute(ack,"id")
                        .setEscaped(commandId);

                    struct timespec time;
                    if (!clock_gettime(CLOCK_REALTIME, &time))
                        XmlElement::Attribute(ack, "time") << time;

                    commandId.clear();
                }
            }
        }

        // Collect all asynchronous events while holding mutex
        ParameterSet cp;
        BroadcastList bl;
        std::queue<ParameterData*> pmd;
        if (polite) {
            pthread::MutexLock lock(mutex);
            changedParameter.clear();
            aic.clear();
            while (!broadcastList.empty()) {
                delete broadcastList.front();
                broadcastList.pop_front();
            }
            while (!parameterMonitorData.empty()) {
                delete parameterMonitorData.front();
                parameterMonitorData.pop();
            }
        }
        else {
            // Create an environment for mutex lock. This lock should be kept
            // as short as possible, and especially not when writing to the
            // output stream
            pthread::MutexLock lock(mutex);

            if (aicDelay)
                --aicDelay;

            ParameterSet::iterator it2, it = changedParameter.begin();
            while (it != changedParameter.end()) {
                it2 = it++;
                if (!aicDelay or aic.find((*it2)->mainParam) == aic.end()) {
                    cp.insert(*it2);
                    changedParameter.erase(it2);
                }
            }

            while (!parameterMonitorData.empty()) {
                ParameterData* d = parameterMonitorData.front();
                if (parameterMonitor
                        or (parameterMonitorSet.find(d->parameter)
                            != parameterMonitorSet.end()))
                    pmd.push(d);
                else
                    delete d;
                parameterMonitorData.pop();
            }

            std::swap(broadcastList, bl);
        }

        // Write all asynchronous events to the client
        {
            for ( ParameterSet::iterator it = cp.begin();
                    it != cp.end(); ++it) {
                XmlElement pu(createElement("pu"));
                XmlElement::Attribute(pu, "index") << (*it)->index;
            }

            while (!pmd.empty()) {
                ParameterData* d = pmd.front();

                XmlElement xml(createElement("parameter"));
                bool shortReply =
                    !knownVariables.insert(d->parameter->variable).second;
                d->parameter->setXmlAttributes(xml, d->data, d->time,
                        shortReply, true, false, 16);
                XmlElement::Attribute(xml, "pm") << 1;
                delete d;
                pmd.pop();
            }

            for ( BroadcastList::const_iterator it = bl.begin();
                    it != bl.end(); ++it) {

                XmlElement broadcast(createElement("broadcast"));

                XmlElement::Attribute(broadcast, "time") << (*it)->ts;

                if (!(*it)->action.empty())
                    XmlElement::Attribute(broadcast, "action")
                        .setEscaped((*it)->action);

                if (!(*it)->message.empty())
                    XmlElement::Attribute(broadcast, "text")
                        .setEscaped((*it)->message);

                delete *it;
            }
        }

        for (SubscriptionManagerVector::iterator it = subscriptionManager.begin();
                it != subscriptionManager.end(); ++it)
            (*it)->rxPdo(quiet);

        while (Event::toXml(this, main->getEvent(eventId)))
            eventId++;
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::processCommand(const XmlParser* parser)
{
    const char *command = parser->tag();
    size_t commandLen = strlen(command);

    static const struct {
        size_t len;
        const char *name;
        void (Session::*func)(const XmlParser*);
    } cmds[] = {
        // First list most common commands
        { 4, "ping",                    &Session::ping                  },
        { 2, "rs",                      &Session::readStatistics        },
        { 2, "wp",                      &Session::writeParameter        },
        { 2, "rp",                      &Session::readParameter         },
        { 4, "xsad",                    &Session::xsad                  },
        { 4, "xsod",                    &Session::xsod                  },
        { 4, "xsap",                    &Session::xsap                  },
        { 4, "xsop",                    &Session::xsop                  },
        { 4, "echo",                    &Session::echo                  },

        // Now comes the rest
        { 2, "rc",                      &Session::readChannel           },
        { 2, "rk",                      &Session::readChannel           },
        { 3, "rpv",                     &Session::readParamValues       },
        { 4, "list",                    &Session::listDirectory         },
        { 4, "auth",                    &Session::authenticate          },
#ifdef GNUTLS_FOUND
        { 8, "starttls",                &Session::startTLS              },
#endif
        { 9, "broadcast",               &Session::broadcast             },
        {11, "remote_host",             &Session::remoteHost            },
        {12, "read_kanaele",            &Session::readChannel           },
        {12, "read_statics",            &Session::readStatistics        },
        {14, "read_parameter",          &Session::readParameter         },
        {15, "read_statistics",         &Session::readStatistics        },
        {15, "message_history",         &Session::messageHistory        },
        {15, "write_parameter",         &Session::writeParameter        },
        {17, "read_param_values",       &Session::readParamValues       },
        {0,  0,                         0},
    };

    // Go through the command list
    for (size_t idx = 0; cmds[idx].len; idx++) {
        // Check whether the lengths fit and the string matches
        if (commandLen == cmds[idx].len
                and !strcmp(cmds[idx].name, command)) {

            LOG4CPLUS_TRACE_STR(server->log,
                    LOG4CPLUS_C_STR_TO_TSTRING(cmds[idx].name));

            // Call the method
            (this->*cmds[idx].func)(parser);

            // Finished
            return;
        }
    }

    LOG4CPLUS_WARN(server->log,
            LOG4CPLUS_TEXT("Unknown command <")
            << LOG4CPLUS_C_STR_TO_TSTRING(command)
            << LOG4CPLUS_TEXT(">"));


    // Unknown command warning
    XmlElement warn(createElement("warn"));
    XmlElement::Attribute(warn, "num") << 1000;
    XmlElement::Attribute(warn, "text") << "unknown command";
    XmlElement::Attribute(warn, "command").setEscaped(command);
}

/////////////////////////////////////////////////////////////////////////////
void Session::broadcast(const XmlParser* parser)
{
    struct timespec ts;
    std::string action, text;

    main->gettime(&ts);
    parser->getString("action", action);
    parser->getString("text", text);

    server->broadcast(this, ts, action, text);
}

/////////////////////////////////////////////////////////////////////////////
void Session::echo(const XmlParser* parser)
{
    echoOn = parser->isTrue("value");
}

/////////////////////////////////////////////////////////////////////////////
void Session::ping(const XmlParser* /*parser*/)
{
    struct timespec time;

    XmlElement e(createElement("ping"));
    if (!clock_gettime(CLOCK_REALTIME, &time))
        XmlElement::Attribute(e, "time") << time;
}

/////////////////////////////////////////////////////////////////////////////
void Session::readChannel(const XmlParser* parser)
{
    const Channel *c = 0;
    bool shortReply = parser->isTrue("short");
    std::string name;
    unsigned int index;

    if (parser->getString("name", name)) {
        c = server->find<Channel>(name);
        if (!c)
            return;
    }
    else if (parser->getUnsigned("index", index)) {
        c = server->getChannel(index);
        if (!c)
            return;
    }

    bool hex = parser->isTrue("hex");

    // A single signal was requested
    if (c) {
        char buf[c->signal->memSize];
        struct timespec time;
        int rv = static_cast<const PdServ::Variable*>(c->signal)
            ->getValue(this, buf, &time);

        XmlElement channel(createElement("channel"));
        c->setXmlAttributes(
                channel, shortReply, hex, false, rv ? 0 : buf, 16, &time);

        if (!shortReply)
            knownVariables.insert(c->signal);

        return;
    }

    // A list of all channels
    const Server::Channels& chanList = server->getChannels();
    XmlElement channels(createElement("channels"));
    for (Server::Channels::const_iterator it = chanList.begin();
            it != chanList.end(); it++) {
        c = *it;
        if (c->hidden)
            continue;

        XmlElement el(channels.createChild("channel"));
        c->setXmlAttributes( el, shortReply, false, false, 0, 16, 0);

        if (!shortReply)
            knownVariables.insert(c->signal);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::listDirectory(const XmlParser* parser)
{
    const char *path;

    if (!parser->find("path", &path))
        return;

    bool hex = parser->isTrue("hex");
    bool noderived = false;
    parser->getBool("noderived", &noderived);

    XmlElement element(createElement("listing"));
    server->listDir(this, element, path, hex, !noderived);
}

/////////////////////////////////////////////////////////////////////////////
void Session::readParameter(const XmlParser* parser)
{
    bool shortReply = parser->isTrue("short");
    bool hex = parser->isTrue("hex");
    std::string name;
    unsigned int index;

    const Parameter *p = 0;
    if (parser->getString("name", name)) {
        p = server->find<Parameter>(name);
        if (!p)
            return;
    }
    else if (parser->getUnsigned("index", index)) {
        p = server->getParameter(index);
        if (!p)
            return;
    }

    if (p) {
        char buf[p->mainParam->memSize];
        struct timespec ts;

        p->mainParam->getValue(this, buf, &ts);

        std::string id;
        parser->getString("id", id);

        XmlElement xml(createElement("parameter"));
        p->setXmlAttributes(xml, buf, ts, shortReply, hex, false, 16);

        if (!shortReply)
            knownVariables.insert(p->mainParam);

        return;
    }

    XmlElement parametersElement(createElement("parameters"));

    const Server::Parameters& parameters = server->getParameters();
    Server::Parameters::const_iterator it = parameters.begin();
    while ( it != parameters.end()) {
        const PdServ::Parameter* mainParam = (*it)->mainParam;
        char buf[mainParam->memSize];
        struct timespec ts;

        if ((*it)->hidden) {
            ++it;
            continue;
        }

        mainParam->getValue(this, buf, &ts);

        while (it != parameters.end() and mainParam == (*it)->mainParam) {
            XmlElement xml(parametersElement.createChild("parameter"));
            (*it++)->setXmlAttributes(
                    xml, buf, ts, shortReply, hex, false, 16);
        }

        if (!shortReply)
            knownVariables.insert(mainParam);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::readParamValues(const XmlParser* /*parser*/)
{
    XmlElement param_values(createElement("param_values"));
    XmlElement::Attribute values(param_values, "value");

    const Server::Parameters& parameters = server->getParameters();
    Server::Parameters::const_iterator it = parameters.begin();
    while ( it != parameters.end()) {
        const PdServ::Parameter* mainParam = (*it)->mainParam;
        char buf[mainParam->memSize];
        struct timespec ts;

        mainParam->getValue(this, buf, &ts);

        if (it != parameters.begin())
            values << ';';
        values.csv(*it, buf, 1, 16);

        while (it != parameters.end() and mainParam == (*it)->mainParam)
            ++it;
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::messageHistory(const XmlParser* parser)
{
    uint32_t seqNo;

    if (parser->getUnsigned("seq", seqNo)) {
        Event::toXml(this, main->getEvent(seqNo));
    } else {
        // <message_history>
        //   ... list of all active messages
        // </message_history>
        typedef std::list<PdServ::EventData> EventList;
        EventList list;

        main->getActiveEvents(&list);

        XmlElement xml(createElement("message_history"));
        for (EventList::iterator it = list.begin();
                it != list.end(); ++it)
            Event::toXml(this, *it, &xml);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::readStatistics(const XmlParser* /*parser*/)
{
    // <clients>
    //   <client name="lansim"
    //           apname="Persistent Manager, Version: 0.3.1"
    //           countin="19908501" countout="27337577"
    //           connectedtime="1282151176.659208"/>
    //   <client index="1" .../>
    // </clients>
    typedef std::list<PdServ::SessionStatistics> StatList;
    StatList stats;
    server->getSessionStatistics(stats);

    XmlElement clients(createElement("clients"));
    for (StatList::const_iterator it = stats.begin();
            it != stats.end(); it++) {
        XmlElement client(clients.createChild("client"));
        XmlElement::Attribute(client,"name")
            .setEscaped((*it).remote.size() ? (*it).remote : "unknown");
        XmlElement::Attribute(client,"apname")
            .setEscaped((*it).client.size() ? (*it).client : "unknown");
        XmlElement::Attribute(client,"countin") << (*it).countIn;
        XmlElement::Attribute(client,"countout") << (*it).countOut;
        XmlElement::Attribute(client,"connectedtime") << (*it).connectedTime;
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::startTLS(const XmlParser* /*parser*/)
{
    createElement("tls");
    xmlstream.flush();

    PdServ::Session::startTLS();
}

/////////////////////////////////////////////////////////////////////////////
void Session::remoteHost(const XmlParser* parser)
{
    parser->getString("name", remoteHostName);

    parser->getString("applicationname", client);

    if (parser->find("access"))
        writeAccess = parser->isEqual("access", "allow")
            or parser->isTrue("access");

    // Check whether stream should be polite, i.e. not send any data
    // when not requested by the client.
    // This is used for passive clients that do not check their streams
    // on a regular basis causing the TCP stream to congest.
    parser->getBool("polite", &polite);

    LOG4CPLUS_INFO(server->log,
            LOG4CPLUS_TEXT("Logging in ")
            << LOG4CPLUS_STRING_TO_TSTRING(remoteHostName)
            << LOG4CPLUS_TEXT(" application ")
            << LOG4CPLUS_STRING_TO_TSTRING(client)
            << LOG4CPLUS_TEXT(" writeaccess=")
            << writeAccess);
}

/////////////////////////////////////////////////////////////////////////////
void Session::writeParameter(const XmlParser* parser)
{
    if (!writeAccess) {
        XmlElement warn(createElement("warn"));
        XmlElement::Attribute(warn, "text") << "No write access";
        return;
    }

    const Parameter *p = 0;

    unsigned int index;
    std::string name;
    if (parser->getString("name", name)) {
        p = server->find<Parameter>(name);
    }
    else if (parser->getUnsigned("index", index)) {
        p = server->getParameter(index);
    }

    if (!p)
        return;

    unsigned int startindex = 0;
    if (parser->getUnsigned("startindex", startindex)) {
        if (startindex >= p->dim.nelem)
            return;
    }

    if (parser->isTrue("aic"))
        server->setAic(p);

    int errnum;
    const char *s;
    if (parser->find("hexvalue", &s)) {
        errnum = p->setHexValue(this, s, startindex);
    }
    else if (parser->find("value", &s)) {
        errnum = p->setDoubleValue(this, s, startindex);
    }
    else
        return;

    if (errnum) {
        // If an error occurred, tell this client to reread the value
        char buf[p->memSize];
        struct timespec time;
        p->mainParam->getValue(this, buf, &time);
        parameterChanged(p, buf, &time);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::xsad(const XmlParser* parser)
{
    unsigned int reduction, blocksize, precision, group;
    bool base64 = parser->isEqual("coding", "Base64");
    bool event = parser->isTrue("event");
    bool foundReduction = false;
    std::list<unsigned int> indexList;
    const Server::Channels& channel = server->getChannels();

    if (parser->isTrue("sync")) {
        for (SubscriptionManagerVector::iterator it = subscriptionManager.begin();
                it != subscriptionManager.end(); ++it)
            (*it)->sync();
        quiet = false;
    }
    else {
        // Quiet will stop all transmission of <data> tags until
        // sync is called
        quiet = parser->isTrue("quiet");
    }

    if (!parser->getUnsignedList("channels", indexList))
        return;

    if (parser->getUnsigned("reduction", reduction)) {
        if (!reduction) {
            XmlElement warn(createElement("warn"));
            XmlElement::Attribute(warn, "command") << "xsad";
            XmlElement::Attribute(warn, "text")
                << "specified reduction=0, choosing reduction=1";

            reduction = 1;
        }

        foundReduction = true;
    }

    // Discover blocksize
    if (event) {
        // blocksize of zero is for event channels
        blocksize = 0;
    }
    else if (parser->getUnsigned("blocksize", blocksize)) {
        if (!blocksize) {
            XmlElement warn(createElement("warn"));
            XmlElement::Attribute(warn, "command") << "xsad";
            XmlElement::Attribute(warn, "text")
                << "specified blocksize=0, choosing blocksize=1";

            blocksize = 1;
        }
    }
    else {
        // blocksize was not supplied, possibly human input
        blocksize = 1;
    }

    if (!parser->getUnsigned("precision", precision))
        precision = 16;

    if (!parser->getUnsigned("group", group))
        group = 0;

    for (std::list<unsigned int>::const_iterator it = indexList.begin();
            it != indexList.end(); it++) {
        if (*it >= channel.size())
            continue;

        const Channel *c = channel[*it];
        const PdServ::Signal *mainSignal = c->signal;

        if (event) {
            if (!foundReduction)
                // If user did not supply a reduction, limit to a
                // max of 10Hz automatically
                reduction = static_cast<unsigned>(
				0.1/mainSignal->sampleTime() + 0.5);
        }
        else if (!foundReduction) {
            // Quite possibly user input; choose reduction for 1Hz
            reduction = static_cast<unsigned>(
                    1.0/mainSignal->sampleTime() / blocksize + 0.5);
        }

        subscriptionManager[c->signal->task->index]->subscribe(
                c, group, reduction, blocksize, base64, precision);
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::xsod(const XmlParser* parser)
{
    std::list<unsigned int> intList;

    //cout << __LINE__ << "xsod: " << endl;

    if (parser->getUnsignedList("channels", intList)) {
        const Server::Channels& channel = server->getChannels();
        unsigned int group;

        if (!parser->getUnsigned("group", group))
            group = 0;

        for (std::list<unsigned int>::const_iterator it = intList.begin();
                it != intList.end(); it++) {
            if (*it < channel.size()) {
                size_t taskIdx = channel[*it]->signal->task->index;
                subscriptionManager[taskIdx]->unsubscribe(channel[*it], group);
            }
        }
    }
    else
        for (SubscriptionManagerVector::iterator it = subscriptionManager.begin();
                it != subscriptionManager.end(); ++it)
            (*it)->clear();
}

/////////////////////////////////////////////////////////////////////////////
void Session::xsap(const XmlParser* parser)
{
    pthread::MutexLock lock(mutex);

    if (parser->isTrue("monitor"))
        parameterMonitor = true;

    std::list<unsigned int> indexList;
    if (parser->getUnsignedList("parameters", indexList)) {
        const Server::Parameters& parameter = server->getParameters();

        for (std::list<unsigned int>::const_iterator it = indexList.begin();
                it != indexList.end(); it++) {
            unsigned int idx = *it;
            if (idx < parameter.size())
                parameterMonitorSet.insert(parameter[idx]);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::xsop(const XmlParser* parser)
{
    pthread::MutexLock lock(mutex);

    std::list<unsigned int> indexList;
    if (!parser->getBool("monitor", &parameterMonitor)
            and !parser->getUnsignedList("parameters", indexList)) {
        parameterMonitor = false;
        parameterMonitorSet.clear();
    }
    else {
        const Server::Parameters& parameter = server->getParameters();

        for (std::list<unsigned int>::const_iterator it = indexList.begin();
                it != indexList.end(); it++) {
            unsigned int idx = *it;
            if (idx < parameter.size())
                parameterMonitorSet.erase(parameter[idx]);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
void Session::authenticate(const XmlParser* parser)
{
    XmlElement auth(createElement("saslauth"));

    const char* mech = 0;
    const char* clientdata = 0;
    bool logout = false;

    if (parser) {
        parser->find("clientdata", &clientdata);
        parser->find("mech", &mech);
        logout = parser->isTrue("logout");
    }

    if (mech or clientdata) {
        const std::string* serverdata;
        bool result = saslProcess(mech, clientdata, &serverdata);

        if (result or !serverdata)
            XmlElement::Attribute(auth, "success") << result;

        if (serverdata)
            XmlElement::Attribute(auth, "serverdata") << *serverdata;
    }
    else if (logout) {
        saslDispose();
    }
    else {
        const char *mechs = saslMechanisms();

        if (mechs)
            XmlElement::Attribute(auth, "mechlist") << mechs;
        else
            XmlElement::Attribute(auth, "success") << 0;
    }
}

/////////////////////////////////////////////////////////////////////////////
XmlElement Session::createElement(const char* name)
{
    return XmlElement(name, xmlstream, 0, &commandId);
}

///////////////////////////////////////////////////////////////////////////
std::string Session::peerAddr(char sep) const
{
    return getAddr(sep);
}

///////////////////////////////////////////////////////////////////////////
std::string Session::localAddr(char sep) const
{
    return server->getAddr(sep);
}

/////////////////////////////////////////////////////////////////////////////
ssize_t Session::read(void* buf, size_t count)
{
    int rv = readData(buf, count);
    if (rv > 0)
        inBytes += rv;
    return rv;
}

/////////////////////////////////////////////////////////////////////////////
ssize_t Session::write(const void* buf, size_t count)
{
    int rv = writeData(buf, count);
    if (rv > 0)
        outBytes += rv;
    return rv;
}
