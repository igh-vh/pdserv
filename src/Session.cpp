/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Session.h"
#include "Main.h"
#include "Debug.h"

#include <sasl/saslutil.h>
#include <cerrno>
#include <cstring>      // strerror()
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#ifdef GNUTLS_FOUND
#    include <gnutls/x509.h>
#    include "TLS.h"
#endif

using namespace PdServ;

// Initialize SASL server. There are no callbacks, as well as an
// empty name so that no default configuration file is searched for
const int Session::saslServerInit = sasl_server_init(NULL, NULL);

/////////////////////////////////////////////////////////////////////////////
Session::Session(const Main *m, log4cplus::Logger& log, size_t bufsize)
: main(m), log(log)
{
    conn = 0;
    p_eof = false;
    state = NoTLS;

#ifdef GNUTLS_FOUND
    tls_session = 0;
#endif

    main->gettime(&connectedTime);
    main->prepare(this);

    char* buf = new char[bufsize];
    setp(buf, buf + bufsize);
}

/////////////////////////////////////////////////////////////////////////////
Session::~Session()
{
#ifdef GNUTLS_FOUND
    if (tls_session)
        gnutls_deinit(tls_session);
#endif

    saslDispose();

    main->cleanup(this);
    delete[] pbase();
}

/////////////////////////////////////////////////////////////////////////////
bool Session::saslInit()
{
    std::string remoteip = peerAddr(';');
    std::string localip = localAddr(';');

    LOG4CPLUS_DEBUG(log,
            LOG4CPLUS_TEXT("Creating new login session for remoteip=")
            << LOG4CPLUS_STRING_TO_TSTRING(remoteip)
            << LOG4CPLUS_TEXT(" localip=")
            << LOG4CPLUS_STRING_TO_TSTRING(localip));

    // Make sure sasl_server_init() was successful
    if (saslServerInit != SASL_OK) {
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Could not initialize SASL server: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(
                    sasl_errstring(saslServerInit, NULL, NULL)));
        return true;
    }

    // Setup callbacks. Only use SASL_CB_GETOPT
    typedef int (*cb_t)(void);
    static const sasl_callback_t _cb[CB_SIZE] = {
        {SASL_CB_LOG,      cb_t((void*)sasl_log),       0},
        {SASL_CB_GETOPT,   cb_t((void*)sasl_getopt),    0},
        {SASL_CB_LIST_END, 0,                           0},
    };
    for (size_t i = 0; i < CB_SIZE-1; ++i) {
        sasl_callbacks[i] = _cb[i];
        sasl_callbacks[i].context = this;
    }
    sasl_callbacks[0].context = const_cast<log4cplus::Logger*>(&log);

    // Create a new login session
    int result = sasl_server_new("pdcom",
                NULL,       /* serverFQDN; null = use gethostname() */
                NULL,       /* user realm, null = use gethostname() */
                localip.c_str(),   /* Local IP */
                remoteip.c_str(),  /* Remote IP */
                sasl_callbacks,
                SASL_SUCCESS_DATA,
                &conn);
    if (result != SASL_OK) {
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Could not create new SASL server instance: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(sasl_errstring(result,0,0)));
        return true;
    }

    return false;
}

/////////////////////////////////////////////////////////////////////////////
const char* Session::saslMechanisms()
{
    if (!conn and saslInit())
        return 0;

    // Send the mechanisms back.
    // This is a space separated list of words, no prefix or suffix
    const char* mechanisms;
    int result = sasl_listmech(conn,
                NULL,   // user
                NULL,   // prefix
                NULL,   // space
                NULL,   // suffix
                &mechanisms,
                NULL,   // plen
                NULL);  // pcount
    if (SASL_OK != result) {
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Could not create new SASL server instance: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(sasl_errstring(result,0,0)));
        return NULL;
    }
    LOG4CPLUS_DEBUG(log,
            LOG4CPLUS_TEXT("Available SASL authentication mechanisms: ")
            << LOG4CPLUS_C_STR_TO_TSTRING(mechanisms));

    return mechanisms;
}

/////////////////////////////////////////////////////////////////////////////
void Session::saslDispose()
{
    sasl_dispose(&conn);
    conn = 0;
}

/////////////////////////////////////////////////////////////////////////////
bool Session::saslProcess(
        const char* mech, const char* response, const std::string** serverout)
{
    int result;
    const char* out;
    unsigned int outlen;

    unsigned int inlen = response ? strlen(response) : 0;
    unsigned int buflen = inlen/4*3 + 1;
    char clientout[buflen];
    unsigned int clientoutlen;

    *serverout = NULL;

    if (!conn and saslInit())
        return false;

    // First decode client base64 coded response
    result = sasl_decode64(response, inlen, clientout, buflen, &clientoutlen);
    if (SASL_OK != result) {
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Could not decode client base64 coded string: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(sasl_errstring(result,0,0)));
        return false;
    }

    if (clientoutlen)
        LOG4CPLUS_TRACE(log,
                LOG4CPLUS_TEXT("SASL: Receive client reply: ")
                << LOG4CPLUS_STRING_TO_TSTRING(
                    std::string(clientout,clientoutlen)));

    // If mech is not null, this is the first step in authentication
    // otherwise it is another step
    if (mech) {
        LOG4CPLUS_DEBUG(log,
                LOG4CPLUS_TEXT("Login: using mechanism ")
                << LOG4CPLUS_C_STR_TO_TSTRING(mech));
        result = sasl_server_start(conn, mech,
                response ? clientout : 0, clientoutlen,
                &out, &outlen);
    }
    else {
        result = sasl_server_step(conn,
                clientout, clientoutlen,
                &out, &outlen);
    }

    LOG4CPLUS_DEBUG(log,
            LOG4CPLUS_TEXT("Login: result ")
            << result << ' ' << (void*)out << ' ' << outlen);

    if (result == SASL_OK) {
        const char* str;
        const void** strp = (const void**)&str;

        const char* userName =
            (SASL_OK == sasl_getprop(conn, SASL_USERNAME, strp) and str)
            ? str : "unknown_user";

        LOG4CPLUS_INFO(log,
                LOG4CPLUS_TEXT("Successfully logged in ")
                << LOG4CPLUS_C_STR_TO_TSTRING(userName));

        if (log.isEnabledFor(log4cplus::DEBUG_LOG_LEVEL)) {
            std::ostringstream os;

            if (SASL_OK == sasl_getprop(conn, SASL_AUTHUSER, strp) and str)
                os << "     authname=" << str;

            if (SASL_OK == sasl_getprop(conn, SASL_MECHNAME, strp) and str)
                os << " mechanism=" << str;

            if (SASL_OK == sasl_getprop(conn, SASL_DEFUSERREALM, strp) and str)
                os << " realm=" << str;

            if (SASL_OK == sasl_getprop(conn, SASL_IPREMOTEPORT, strp) and str)
                os << " remoteip=" << str;

            log.forcedLog(log4cplus::DEBUG_LOG_LEVEL,
                    LOG4CPLUS_STRING_TO_TSTRING(os.str()));
        }
    }
    else if (result != SASL_CONTINUE) {
        const char* str;
        const void** sptr = (const void**)&str;

        const char* userName =
            (SASL_OK == sasl_getprop(conn, SASL_USERNAME, sptr) and str)
            ? str : "unknown_user";

        const char* authSrc =
            (SASL_OK == sasl_getprop(conn, SASL_AUTHSOURCE, sptr) and str)
            ? str : "unknown";

        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Login failed for user ")
                << LOG4CPLUS_C_STR_TO_TSTRING(userName)
                << LOG4CPLUS_TEXT(" using ")
                << LOG4CPLUS_C_STR_TO_TSTRING(authSrc)
                << LOG4CPLUS_TEXT(": ")
                << LOG4CPLUS_C_STR_TO_TSTRING(sasl_errdetail(conn)));
        return false;
    }

    if (outlen) {
        buflen = (outlen+2)/3*4 + 1;
        char outbuf[buflen];
        int rv = sasl_encode64(out, outlen, outbuf, buflen, &buflen);
        if (rv != SASL_OK) {
            LOG4CPLUS_ERROR(log,
                    LOG4CPLUS_TEXT("Could not base64-encode string: ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(sasl_errstring(rv,0,0)));
            return false;
        }
        sasl_reply.assign(outbuf, buflen);
        *serverout = &sasl_reply;
        LOG4CPLUS_TRACE(log,
                LOG4CPLUS_TEXT("SASL: Sending challenge ")
                << LOG4CPLUS_STRING_TO_TSTRING(std::string(out,outlen)));
    }

    return result == SASL_OK;
}

///////////////////////////////////////////////////////////////////////////
// man: sasl_log_t
int Session::sasl_log(void *context, int level,
        const char *message)
{
    log4cplus::LogLevel log4cpluslevel;
    switch (level) {
        case SASL_LOG_NONE:     /* don't log anything */
            log4cpluslevel = log4cplus::OFF_LOG_LEVEL;
            break;

        case SASL_LOG_ERR:      /* log unusual errors (default) */
            log4cpluslevel = log4cplus::FATAL_LOG_LEVEL;
            break;

        case SASL_LOG_FAIL:     /* log all authentication failures */
            log4cpluslevel = log4cplus::ERROR_LOG_LEVEL;
            break;

        case SASL_LOG_WARN:     /* log non-fatal warnings */
            log4cpluslevel = log4cplus::WARN_LOG_LEVEL;
            break;

        case SASL_LOG_NOTE:     /* more verbose than LOG_WARN */
            log4cpluslevel = log4cplus::INFO_LOG_LEVEL;
            break;

        case SASL_LOG_DEBUG:    /* more verbose than LOG_NOTE */
            log4cpluslevel = log4cplus::DEBUG_LOG_LEVEL;
            break;

        case SASL_LOG_TRACE:    /* traces of internal protocols */
        case SASL_LOG_PASS:     /* traces of internal protocols, including
				 * passwords */
        default:
            log4cpluslevel = log4cplus::TRACE_LOG_LEVEL;
            break;
    }

    log4cplus::Logger* logger = reinterpret_cast<log4cplus::Logger*>(context);
    if (logger and logger->isEnabledFor(log4cpluslevel)) {
        log4cplus::tostringstream os;
        os << LOG4CPLUS_TEXT("SASL: ")
            << LOG4CPLUS_C_STR_TO_TSTRING(message);
        logger->forcedLog(log4cpluslevel, os.str(), __FILE__, __LINE__);
    }

    return SASL_OK;
}

///////////////////////////////////////////////////////////////////////////
// man: sasl_getopt_t
int Session::sasl_getopt(void *context, const char* /*plugin_name*/,
        const char *option, const char **result, unsigned *len)
{
    int rv = SASL_BADPARAM;
    log_debug("context=%p, option=%s, result=%p len=%p",
            context, option, result, len);
    Session* session = reinterpret_cast<Session*>(context);

    if (session and option) {
        SaslParam::iterator it = session->saslParam.find(option);
        if (it == session->saslParam.end()) {
            Config param = session->main->config("access")["sasl"][option];
            if (param)
                it = session->saslParam.insert(
                        std::make_pair(std::string(option), param.toString()))
                    .first;
            else {
                LOG4CPLUS_DEBUG(session->log,
                        LOG4CPLUS_TEXT("SASL parameter not specified: ")
                        << LOG4CPLUS_C_STR_TO_TSTRING(option));
            }
        }
        if (it != session->saslParam.end()) {
            *result = it->second.c_str();
            if (len)
                *len = it->second.size();
            rv = SASL_OK;
        }
    }

//#define STR(x) #x
//#define QUOTE(x) STR(x)
//    if (!strcmp(option, "log_level") and rv != SASL_OK) {
//        *result = QUOTE(SASL_LOG_PASS);
//        log_debug("setting log level rv=%s", *result);
//        rv = SASL_OK;
//    }

    return rv;
}

/////////////////////////////////////////////////////////////////////////////
int Session::startTLS()
{
#ifdef GNUTLS_FOUND
    int result;

    result = gnutls_init(&tls_session, GNUTLS_SERVER);
    if (result) {
        LOG4CPLUS_FATAL(log,
                LOG4CPLUS_TEXT("gnutls_init() failed: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                << LOG4CPLUS_TEXT(" (")
                << LOG4CPLUS_C_STR_TO_TSTRING(
                    gnutls_strerror_name(result))
                << LOG4CPLUS_TEXT(")"));
        return result;
    }

    main->initTlsSessionData(tls_session, &blacklist);

    gnutls_session_set_ptr(tls_session, this);

    gnutls_transport_set_ptr(tls_session, this);
    gnutls_transport_set_push_function(tls_session, gnutls_push_func);
    gnutls_transport_set_pull_function(tls_session, gnutls_pull_func);

    state = InitTLS;

    LOG4CPLUS_INFO_STR(log,
            LOG4CPLUS_TEXT("Started TLS handshake"));

    return 0;
#else
    return -1;
#endif
}

/////////////////////////////////////////////////////////////////////////////
bool Session::eof() const
{
    return p_eof;
}

/////////////////////////////////////////////////////////////////////////////
int Session::overflow(int value)
{
    char c = value;
    return xsputn(&c, 1) ? c : traits_type::eof();
}

/////////////////////////////////////////////////////////////////////////////
std::streamsize Session::xsputn(const char * buf, std::streamsize count)
{
    const char* ptr = buf;

    do {
        // Put data into buffer
        size_t n = std::min(epptr() - pptr(), count);
        std::copy(ptr, ptr + n, pptr());

        // Update pointers
        pbump(n);
        ptr += n;
        count -= n;

    } while (!(pptr() == epptr() and flush(true)) and count);

    return ptr - buf;
}

/////////////////////////////////////////////////////////////////////////////
int Session::sync()
{
    return flush(false);
}

/////////////////////////////////////////////////////////////////////////////
// Flush output buffer.
//
// partial: true: do only one flush pass
int Session::flush(bool partial)
{
    const char* buf = pbase();
    size_t count = pptr() - buf;

    if (p_eof)
        return -1;
    else if (!count)
        return 0;

//    log_debug("flushing %i %zu", partial, count);
    int result = 0;
    do {
#ifdef GNUTLS_FOUND
        switch (state) {
            case NoTLS:
                result = write(buf, count);
                break;

            case InitTLS:
                // Ignore partial flush request, called from Session::sync()
                if (!partial)
                    return 0;

                LOG4CPLUS_FATAL_STR(log,
                        LOG4CPLUS_TEXT(
                            "Output buffer overflow during TLS handshake"));
                result = -ENOSPC;
                break;

            case RunTLS:
                result = gnutls_record_send(tls_session, buf, count);
                break;
        }
#else
        result = write(buf, count);
#endif

//        log_debug("flushing result %i", result);
        if (result <= 0)
            break;

        buf   += result;
        count -= result;

    } while (count and !partial);

    // Copy remaining data to buffer start and 
    // update std::streambuf's current put pointer pptr()
    std::copy(buf, const_cast<const char*>(pptr()), pbase());
    pbump(pbase() - buf);

    // Calculate EOF
    p_eof |= !result
        or (result < 0 and (state == NoTLS
#ifdef GNUTLS_FOUND
        or gnutls_error_is_fatal(result)
#endif
        ));

    if (!p_eof)
        return 0;

    // Interpret EOF
    if (!result)
        LOG4CPLUS_INFO_STR(log,
                LOG4CPLUS_TEXT("Client closed connection"));
    else if (state != RunTLS)
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("Network write() error: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(strerror(-result)));
#ifdef GNUTLS_FOUND
    else
        LOG4CPLUS_ERROR(log,
                LOG4CPLUS_TEXT("gnutls_record_send(")
                << count
                << LOG4CPLUS_TEXT("): ")
                << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                << LOG4CPLUS_TEXT(" (")
                << LOG4CPLUS_C_STR_TO_TSTRING(
                    gnutls_strerror_name(result))
                << ')');
#endif

    return -1;
}

/////////////////////////////////////////////////////////////////////////////
int Session::underflow()
{
    char c;
    return xsgetn(&c, 1) == 1 ? c : traits_type::eof();
}

/////////////////////////////////////////////////////////////////////////////
std::streamsize Session::xsgetn(char* s, std::streamsize n)
{
    ssize_t result = 0;

#ifdef GNUTLS_FOUND
//    log_debug("tlsstate = %i", state);
    switch (state) {
        case NoTLS:
            result = read(s, n);
            break;

        case InitTLS:
            result = gnutls_handshake(tls_session);
            log_debug("gnutls_handshake = %zi", result);

            // break out while still handshaking
            if (result != GNUTLS_E_SUCCESS)
                break;

            state = RunTLS;
            sync();

            {
                char *info = gnutls_session_get_desc(tls_session);
                LOG4CPLUS_INFO(log,
                        LOG4CPLUS_TEXT("TLS handshake finished: ")
                        << LOG4CPLUS_C_STR_TO_TSTRING(info)
                        );
                gnutls_free(info);
            }

            // return successfully if there is no data available
            if (!gnutls_record_check_pending(tls_session))
                return 0;

            /* FALLTHRU */

        case RunTLS:
            result = gnutls_record_recv(tls_session, s, n);
//            log_debug("gnutls_record_recv = %zi", result);
            break;
    }
#else
    result = read(s, n);
#endif

    if (result > 0)
        return result;

    // Calculate EOF
    if (!result or state == NoTLS
#ifdef GNUTLS_FOUND
            or gnutls_error_is_fatal(result)
#endif
       )
        p_eof = true;

    // Interpret EOF
    if (p_eof) {
        if (!result
#ifdef GNUTLS_FOUND
                or result == GNUTLS_E_PREMATURE_TERMINATION
                or result == GNUTLS_E_UNEXPECTED_PACKET_LENGTH
                or result == GNUTLS_E_SESSION_EOF
#endif
                )
            LOG4CPLUS_INFO_STR(log,
                    LOG4CPLUS_TEXT("Client closed connection"));
        else if (state == NoTLS)
            LOG4CPLUS_ERROR(log,
                    LOG4CPLUS_TEXT("Network error: ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(strerror(-result)));
#ifdef GNUTLS_FOUND
        else {
            const char* func = state == InitTLS
                ? "gnutls_handshake" : "gnutls_record_recv";
            LOG4CPLUS_ERROR(log,
                    LOG4CPLUS_TEXT("TLS error during ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(func)
                    << LOG4CPLUS_TEXT("(): ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                    << LOG4CPLUS_TEXT(" (")
                    << LOG4CPLUS_C_STR_TO_TSTRING(
                        gnutls_strerror_name(result))
                    << ')');
        }
#endif

        result = 0;
    }

    return result;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
#ifdef GNUTLS_FOUND
/////////////////////////////////////////////////////////////////////////////
int Session::gnutls_verify_client(gnutls_session_t tls_session)
{
    int result;
    Session* session =
        reinterpret_cast<Session*>(gnutls_session_get_ptr(tls_session));
    const Blacklist* blacklist = session->blacklist;

    unsigned int list_size = ~0U;
    const gnutls_datum_t* certs =
        gnutls_certificate_get_peers (tls_session, &list_size);
    if (list_size == ~0U or !certs or !list_size) {
        LOG4CPLUS_FATAL_STR(session->log,
                LOG4CPLUS_TEXT(
                    "gnutls_certificate_get_peers() failed: "
                    "Client did not send a certificate or "
                    "there was an error retrieving it"));
        return -1;
    }
    else {
        LOG4CPLUS_INFO(session->log,
                LOG4CPLUS_TEXT("Received ")
                << list_size
                << LOG4CPLUS_TEXT(" certificates from peer"));
    }

    gnutls_x509_crt_t crt;
    result = gnutls_x509_crt_init (&crt);
    if (result) {
        LOG4CPLUS_FATAL(session->log,
                LOG4CPLUS_TEXT("gnutls_x509_crt_init() failed: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                << LOG4CPLUS_TEXT(" (")
                << LOG4CPLUS_C_STR_TO_TSTRING(
                    gnutls_strerror_name(result))
                << LOG4CPLUS_TEXT(")"));
        return -1;
    }

    for (unsigned int i = 0; !result and i < list_size; ++i) {
        result = gnutls_x509_crt_import (crt, certs++, GNUTLS_X509_FMT_DER);
        if (result) {
            LOG4CPLUS_FATAL(session->log,
                    LOG4CPLUS_TEXT("gnutls_x509_crt_import(")
                    << i
                    << LOG4CPLUS_TEXT(") failed: ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                    << LOG4CPLUS_TEXT(" (")
                    << LOG4CPLUS_C_STR_TO_TSTRING(
                        gnutls_strerror_name(result))
                    << LOG4CPLUS_TEXT(")"));
            break;
        }

        gnutls_datum_t cinfo = { NULL, 0 };
        result = gnutls_x509_crt_print(
                crt, GNUTLS_CRT_PRINT_ONELINE, &cinfo);
        if (result == GNUTLS_E_SUCCESS) {
            LOG4CPLUS_INFO(session->log,
                    LOG4CPLUS_TEXT("Certificate[")
                    << i
                    << LOG4CPLUS_TEXT("].info: ")
                    << LOG4CPLUS_C_STR_TO_TSTRING((const char*)cinfo.data));
        }
        else {
            LOG4CPLUS_INFO(session->log,
                    LOG4CPLUS_TEXT("gnutls_x509_crt_print(")
                    << i
                    << LOG4CPLUS_TEXT(") failed: ")
                    << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                    << LOG4CPLUS_TEXT(" (")
                    << LOG4CPLUS_C_STR_TO_TSTRING(
                        gnutls_strerror_name(result))
                    << LOG4CPLUS_TEXT(")"));
        }
        gnutls_free(cinfo.data);

        if (!blacklist->empty()) {
            unsigned char buf[100];
            size_t len = sizeof(buf);
            result = gnutls_x509_crt_get_key_id(crt, 0, buf, &len);
            if (result or !len) {
                LOG4CPLUS_FATAL(session->log,
                        LOG4CPLUS_TEXT("gnutls_x509_crt_get_key_id(")
                        << i
                        << LOG4CPLUS_TEXT(") failed: ")
                        << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                        << LOG4CPLUS_TEXT(" (")
                        << LOG4CPLUS_C_STR_TO_TSTRING(
                            gnutls_strerror_name(result))
                        << LOG4CPLUS_TEXT(")"));
                result = -1;
                break;
            }
            else {
                Blacklist::const_iterator it =
                    blacklist->find(datum_string(buf, len));
                if (it != blacklist->end()) {
                    LOG4CPLUS_FATAL(session->log,
                            LOG4CPLUS_TEXT(
                                "Certificate is blacklisted. Public Key Id = ")
                            << LOG4CPLUS_STRING_TO_TSTRING(std::string(*it)));
                    result = -1;
                    break;
                }
            }
        }
    }

    gnutls_x509_crt_deinit (crt);

    if (result)
        return result;

    unsigned int status;
    result = gnutls_certificate_verify_peers2(tls_session, &status);
    if (result) {
        // Complain
        LOG4CPLUS_FATAL(session->log,
                LOG4CPLUS_TEXT("gnutls_certificate_verify_peers2() failed: ")
                << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                << LOG4CPLUS_TEXT(" (")
                << LOG4CPLUS_C_STR_TO_TSTRING(
                    gnutls_strerror_name(result))
                << LOG4CPLUS_TEXT(")"));
        return -1;
    }
    else if (status) {
        std::ostringstream os;
        os << "Verification of peer's certificate failed: ";
        if (status & GNUTLS_CERT_INVALID)
            os << "Certificate is invalid";

        if (status & GNUTLS_CERT_REVOKED)
            os << "Certificate is revoked";

        if (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
            os << "Certificate signer is not found";

        if (status & GNUTLS_CERT_SIGNER_NOT_CA)
            os << "Certificate signer is not a certification authority";

        if (status & GNUTLS_CERT_INSECURE_ALGORITHM)
            os << "Certificate uses an insecure algorithm";

        if (status & GNUTLS_CERT_NOT_ACTIVATED)
            os << "Certificate is not activated";

        if (status & GNUTLS_CERT_EXPIRED)
            os << "Certificate expired";

        LOG4CPLUS_FATAL_STR(session->log,
                LOG4CPLUS_STRING_TO_TSTRING(os.str()));

        return -1;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////
ssize_t Session::gnutls_push_func(
                gnutls_transport_ptr_t ptr, const void* buf, size_t count)
{
    Session* session = reinterpret_cast<Session*>(ptr);
    ssize_t result = session->write(buf, count);

//    log_debug("result = %zi", result);
    if (result >= 0)
        return result;

    gnutls_transport_set_errno (session->tls_session, -result);

    return -1;
}

/////////////////////////////////////////////////////////////////////////////
ssize_t Session::gnutls_pull_func(
                gnutls_transport_ptr_t ptr, void* buf, size_t count)
{
    Session* session = reinterpret_cast<Session*>(ptr);
    ssize_t result = session->read(buf, count);

//    log_debug("result = %zi", result);
    if (result >= 0)
        return result;

    // Session::read() returns a negative value for errno
    gnutls_transport_set_errno (session->tls_session, -result);

    return -1;
}
#endif
